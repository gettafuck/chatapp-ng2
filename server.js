'use strict';

const path = require('path');
const crypto = require('crypto');

const _ = require('lodash');

const bodyParser = require('body-parser');
const express = require('express');

const saltGenerator = require('./services/saltGenerator');
const defineUserRole = require('./services/defineUserRole');
const config = require('./config');

const app = express();
// const server = app.listen(config.server.port, () => {
// 	console.log('Server listening at ' +
// 		config.server.protocol + config.server.address + ':' + config.server.port);
// });

let serverUrl = config.server.protocol + config.server.address + ':' + config.server.port;


// db
require('./db/db-connect')();
const User = require('./db/models/user');


// Socket.io
	let server = require('http').createServer(app);
	let io = require('socket.io')(server);

	let socketChatDependencies = {
		io: io
	}
	require('./socketRoutes/socketChat')(socketChatDependencies);

	server.listen(config.server.port, () => {
		console.log('socket.io server listening at ' + serverUrl);
	});



/*******
* Routes *
********/

/** Cross Origin Requests **/
	let allowedHeaders = [
		'Access-Control-Allow-Origin',
		'Content-Type'
	];
	allowedHeaders = allowedHeaders.join(', ');

	let allowedDomains = '*';

	let allowedMethods = [
		'POST',
		'GET',
		'PUT',
		'DELETE'
	];
	allowedMethods = allowedMethods.join(', ');

	app.use((req, res, next) => {
		res.setHeader('Access-Control-Allow-Headers', allowedHeaders);
		res.setHeader('Access-Control-Allow-Origin', allowedDomains);
		res.setHeader('Access-Control-Allow-Methods', allowedMethods);
		return next();
	});


/** others **/
	app.use('/node_modules', express.static(__dirname + '/node_modules'));
	app.use('/client', express.static(__dirname + '/client'));
	app.use('/uploads', express.static(__dirname + '/uploads'));

	app.use(bodyParser.json());


/** Chat **/


/** Users **/

	// ! DELETE TEST ROUTES IN DEPLOYMENT BRANCH
	//-------------//
	// test routes //
	//-------------//

	app.delete('/test/users', (request, response) => {
		User.collection.remove((err) => {
			if (err) {
		  		console.log('/users | DELETE | Error was occurred');
		  		response.status(403).send(err.errmsg);
		  	}
		  	if(!err)
				response.status(200).send('collection users was removed');
		});
	});

	app.post('/test/users/:amount', (request, response) => {
		let amount = request.params.amount * 1;
		let names = ['Dylan', 'Bob', 'Mike', 'Tim', 'Torry', 'Jake', 'Johny', 'Sam', 'Luis', 'Derek', 'Alan'];
		let surNames = ['Smith', 'Doe', 'Rieder', 'Pudwill', 'Lake', 'Li', 'Fill', 'McDonald', 'Ake', 'Anderson', 'Lyhn'];
		let cities = ['London', 'Liverpool', 'New York', 'Tokio', 'Berlin', 'Manchester', 'Dublin', 'Oslo', 'Stokholm', 'Amsterdam', 'Dortmut'];
		let getRandInt = (min, max) => {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		let password = 'Password99';
		let newSalt = saltGenerator(100);
		let saltedPassword = password + newSalt;
		let hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');


		let users = [];
		for(let i = 0; i < amount; i++) {
			let newUser = {
				name: names[ getRandInt(0, 10) ],
				surName: surNames[ getRandInt(0, 10) ],
				city: cities[ getRandInt(0, 10) ],
				imgUrl: serverUrl + '/uploads/img/users/' + getRandInt(1, 11) + '.jpg',

				login: 'test_login_' + saltGenerator(10) + '_' + saltGenerator( getRandInt(1, 11) ),

				passwordHash: hashedPassword,
				salt: newSalt,
				token: saltGenerator(100),

				role: 'user'
			};
			users.push(newUser);
		}

		User.collection.dropIndex('login_1'); // fix of error
											  // 'E11000 duplicate key error collection: contacts_ng2.users index: login_1 dup key: { : null }'
		User.collection.insert(users, (err, doc) => {
			if(err) {
		  		console.log('/users/:amount | POST | Error was occurred');
				console.log(err.errmsg);
				response.status(403).send(err.errmsg);
			}
			if(!err) {
				response.status(200).send(amount + ' users was created');
			}
		});
	});


	//-------------//
	// work routes //
	//-------------//

	app.post('/login', (request, response) => {
		let userData = request.body;
		User.findOne({ login: userData.login }, (err, user) => {
			if (err) response.send(err.errmsg);
		  	else {
				if(user) {
					let saltedPassword = userData.password + user.salt;
					let hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');

					if(hashedPassword === user.passwordHash) {
						let newToken = saltGenerator(100);
						user.token = newToken;
						user.passwordHash = undefined;
						user.salt = undefined;

						User.update({ _id: user._id }, { token: newToken }, (err) => {
							if(err) response.send(err.errmsg);
							else response.status(200).send(user);
						});
					} else response.status(403).send('Wrong password.');
					
				} else response.status(403).send('Wrong login.');
			}
		});
	});

	app.post('/logout', (request, response) => {
		let token = request.body.token;

		User.findOne({ token: token }, (err, user) => {
			if (err) response.send(err.errmsg);
		  	else {
				if(user) {
					User.update({ token: token }, { token: null }, (err) => {
						if(err) response.status(500).send(err.errmsg);
						else response.status(200).send('User was logout.');
					});
				} else {response.status(403).send('Wrong token.');}
			}
		});
	});

	app.post('/user', (request, response) => {
		let userData = request.body;

		let newSalt = saltGenerator(100);
		let saltedPassword = userData.password + newSalt;
		let hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');

		userData.passwordHash = hashedPassword;
		userData.salt = newSalt;
		delete userData.password;
		userData.token = saltGenerator(100);

		// TODO: vulnerability -> only admin can create users with role 'admin' and 'moderator'
		if(!userData.role)
			userData.role = 'user';

		if(!userData.imgUrl)
			userData.imgUrl =  serverUrl + '/uploads/img/defaults/default-user-logo.jpg';

		User.findOne({login: userData.login}, (err, doc) => {
			if(doc) {
				response.status(403).send({
					msg: 'User with such login is existed',
					notUniqueLogin: true
				});
			} else {
				let newUser = new User(userData);
				newUser.save((err, doc) => {
					if(err) {
				  		console.log('/user | POST | Error was occurred');
						console.log(err.errmsg);
						response.status(403).send(err.errmsg);
					}
					if(doc) {
						response.send(doc._id);
					}
				});
			}
		});
		
	});

	app.get('/user/:id', (request, response) => {
		let id = request.params.id;
		User.find({ _id: id }, (err, docs) => {
			if (err) {
		  		console.log('/user/:id | GET | Error was occurred');
				console.log(err.errmsg);
		  		response.send(err.errmsg);
		  	}
			if(docs) {
				let doc = docs[0] || {};
				// delete hidden properties
				doc.login = undefined;
				doc.passwordHash = undefined;
				doc.salt = undefined;
				doc.token = undefined;
				response.send(doc);
			}
		});
	});

	app.get('/users', (request, response) => {
		User.find()
		.sort([['registrationDate', -1]]).exec((err, docs) => {
			if (err) {
		  		console.log('/users | GET | Error was occurred');
		  		response.send(err.errmsg);
		  	}
		  	if(docs) {
		  		// delete hidden properties
		  		let i = 0;
		  		_.forEach(docs, (el) => {
		  			el.login = undefined;
		  			el.passwordHash = undefined;
		  			el.salt = undefined;
		  			el.token = undefined;

		  			// TODO: only owner of contacts can see contacts. Owner can allow to display his contacts to all registered users
		  			el.contacts = undefined;

		  			i++;
		  			if(i === docs.length)
		  				response.send(docs);
		  		});
		  		if(docs.length === 0)
		  			response.send(docs);
		  	}
		});
	});

	app.put('/user/:id', (request, response) => {
		let id = request.params.id || '';
		let token = request.body.token || '';
		let userData = request.body;
		delete userData._id;
		delete userData.token;

		let params = {
			token: token,
			id: id,
			UserEntity: User
		}
		defineUserRole(params, (role) => {
			if(role === 'admin' || role === 'dataCreator') {
				User.update({ _id: id }, userData, (err) => {
					if(err) {
						let msg = err.errmsg + '\n' + '/user/:id | PUT | Error was occurred';
				  		console.log(msg);
				  		response.status(403).send(msg);
					} else {
						response.status(200).send(id);
					}
				});
			} else {
				response.status(403).send('Update access forbidden.');
			}
		});
	});

	app.delete('/user/:id', (request, response) => {
		let id = request.params.id;
		let _token = request.body.token;
		request.body.token = undefined;

		authCheck(_token, (accessType) => {

			if((accessType == 'admin') || (accessType == 'currentUser')){
				User.remove({ _id: id }, (err, doc) => {
					if (err) {
				  		console.log('/user/:id | DELETE | Error was occurred');
				  		console.log(err.errmsg);
				  		response.send(err.errmsg);
				  	} else {
						response.send(id);
					}
				});
			} else {
				response.send('Wrong access rights');
			}
		}, id);
	});
