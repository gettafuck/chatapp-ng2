'use strict';

var Config = {
	server: {
		protocol: 'http://',
		address: 'localhost',
		port: 2222
	}, 
	db: {
		address: 'mongodb://localhost',
		port: '27017',
		name: 'chatapp_ng2'
	}
};

module.exports = Config;