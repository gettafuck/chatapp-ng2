import {Injectable} from '@angular/core';
Injectable();

declare var $: any;

export class UI {
	public initSlimsroll() {
        $('.slimScrollable_light').slimScroll({
    		height: 'auto',
    		color: 'rgba(255,255,255, .9)',
    		size: '5px'
    	});
        $('.slimScrollable_light_alwaysVisible').slimScroll({
    		height: 'auto',
    		color: 'rgba(255,255,255, .9)',
    		size: '5px',
    		alwaysVisible: true
    	});

        $('.slimScrollable_dark').slimScroll({
    		height: 'auto',
    		color: 'rgba(0,0,0, .9)',
    		size: '5px'
    	});
        $('.slimScrollable_dark_alwaysVisible').slimScroll({
    		height: 'auto',
    		color: 'rgba(0,0,0, .9)',
    		size: '5px',
    		alwaysVisible: true
    	});

        $('.routePage').slimScroll({
    		width: '100%',
    		height: '100%',
    		color: 'rgba(255,255,255, .9)',
    		size: '5px',
    		alwaysVisible: true
    	});
		console.log('✓ inintialized slimScroll elements.');
	}
	public initMaterializeComponents() {
		$('.collapsible').collapsible();
		console.log('✓ inintialized componets of MaterializeCSS.');
	}

	public toggleClass(blockId:string, classForToggle:string, delay:number = 1) {
		setTimeout(() => {
			let block = document.getElementById(blockId);
			block.classList.toggle(classForToggle);
		}, delay);
	}

	public toggleMenu() {
		this.toggleClass('nav-menu', 'nav-menu_active');
		$('.changeable-onMenuOpen').toggleClass('moveLeft-onMenuOpen');
		$('home-component .slimScrollDiv').toggleClass('moveLeft-onMenuOpen');
	}

	public handleMenuItem(pageId:string) {
		console.log('handleMenuItem - page id: ' + pageId);
		setTimeout(() => {
			$('#'+pageId).addClass('activePage');
		}, 200);

		if( $('#nav-menu').hasClass('nav-menu_active') ) {
			setTimeout(() => {
				$('.changeable-onMenuOpen').addClass('moveLeft-onMenuOpen');

				$('home-component .slimScrollDiv').addClass('moveLeft-onMenuOpen');
			}, 200);
		}
	}

	public scrollTo(target:string, time:number) {
	    if(!time) time = 300;
	    target = '#' + target;
	    $('html, body').animate({scrollTop: $(target).offset().top}, time);
	    return false;
	}
}