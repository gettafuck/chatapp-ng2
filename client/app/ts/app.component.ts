import {RouterModule} from '@angular/router';
import {routing, appRoutingProviders}  from './app.routing';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


import { Http, HttpModule, Response, Headers, RequestOptions } from '@angular/http';

import {Component, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {FormsModule} from '@angular/forms';

/*
 * Components
 */
import {NavbarComponent} from './components/navbar/navbar.component';

import {HomePageComponent} from './components/routepages/home/home.component';
import {LoginPageComponent} from './components/routepages/login/login.component';
import {RegisterPageComponent} from './components/routepages/register/register.component';
import {InboxPageComponent} from './components/routepages/inbox/inbox.component';

import {ArtPageComponent} from './components/routepages/art/art.component';
import {WritingPageComponent} from './components/routepages/writing/writing.component';
import {SearchPageComponent} from './components/routepages/search/search.component';

import {ChatThreads, ChatThread} from './components/ChatThreads';
import {ChatMessage} from './components/ChatMessage';
import {ChatWindow} from './components/ChatWindow';

/*
 * Injectables
 */
import {servicesInjectables} from './services/services';
import {utilInjectables} from './util/util';

/*
 * Services
 */
import {MessagesService, ThreadsService, UserService} from './services/services';

import {UI} from './services/ui.service';

import {ChatExampleData} from './ChatExampleData';


/*
 * Webpack
 */

@Component({
    selector: 'app-component',
    template: `
        <navbar></navbar>

        <div class="container">
            <!-- <chat-threads></chat-threads> -->
            <chat-window></chat-window>
        </div>

        <router-outlet></router-outlet>
    `
})

@NgModule({
    imports: [ BrowserModule, routing, FormsModule,  HttpModule ],
    declarations: [
        /* root component */
        AppComponent,

        NavbarComponent,
        /* route pages components */
        HomePageComponent,
        LoginPageComponent,
        RegisterPageComponent,
        InboxPageComponent,

        ArtPageComponent,
        WritingPageComponent,
        SearchPageComponent,

        /* chat components */
        ChatThreads,
        ChatThread,
        ChatWindow,
        ChatMessage
    ],
    providers: [
        MessagesService, ThreadsService, UserService,

        UI,

        appRoutingProviders,
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap: [ AppComponent ]
})

export class AppComponent {
    constructor(public messagesService: MessagesService,
              public threadsService: ThreadsService,
              public userService: UserService) {
        ChatExampleData.init(messagesService, threadsService, userService);
    }
}