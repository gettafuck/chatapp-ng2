// core modules
import {
    Component,
    NgModule,
    OnInit,
    ElementRef,
    ChangeDetectionStrategy
} from '@angular/core';
import {Observable} from 'rxjs';
import {FormsModule}   from '@angular/forms';


// services
import {
    MessagesService,
    ThreadsService,
    UserService
} from '../services/services';
import { UI } from './../services/ui.service';


// models
import {User, Thread, Message} from '../models';


// configs
import {Config} from './../Config';


// constants
declare var $: any;


// outside components
import {ChatMessage} from './ChatMessage';


// current component
@Component({
    selector: 'chat-window',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: require('./ChatWindow.html')
})

@NgModule({
    imports: [FormsModule],
    declarations: [ChatMessage]
})

export class ChatWindow implements OnInit {
    messages: Observable<any>;
    currentThread: Thread;
    draftMessage: Message;
    currentUser: User;

    public ui:UI;

    constructor(ui:UI,
                public messagesService: MessagesService,
                public threadsService: ThreadsService,
                public userService: UserService,
                public el: ElementRef) {
        this.ui = ui;

        $(document).ready(() => {
            console.log('Initializing in ChatWindow component:');
            ui.initSlimsroll();
        });
    }

    ngOnInit(): void {
    this.messages = this.threadsService.currentThreadMessages;

    this.draftMessage = new Message();

    this.threadsService.currentThread.subscribe(
      (thread: Thread) => {
        this.currentThread = thread;
      });

    this.userService.currentUser
      .subscribe(
        (user: User) => {
          this.currentUser = user;
        });

    this.messages
        .subscribe((messages: Array<Message>) => {
            setTimeout(() => {
                // this.scrollToBottom();
                console.log(messages[0].author.name);
            });
        });
    }

    onEnter(event: any): void {
    this.sendMessage();
    event.preventDefault();
    }

    sendMessage(): void {
    let m: Message = this.draftMessage;
    m.author = this.currentUser;
    m.thread = this.currentThread;
    m.isRead = true;
    this.messagesService.addMessage(m);
    this.draftMessage = new Message();
    }

    // scrollToBottom(): void {
    //     let scrollPane: any = this.el
    //         .nativeElement.querySelector('.msg-container-base');
    //     scrollPane.scrollTop = scrollPane.scrollHeight;
    // }
}
