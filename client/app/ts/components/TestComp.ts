import {
	Component,
	NgModule
} from '@angular/core';

@Component({
    selector: 'test-comp',
    template: `
        <h3> Test Comp </h3>
    `
})
export class TestComp {}
