import {
    Component,
    NgModule,
    OnInit,
    ElementRef,
    ChangeDetectionStrategy
} from '@angular/core';

import {FormsModule} from '@angular/forms';

import {
    MessagesService,
    ThreadsService,
    UserService
} from '../services/services';
import {Observable} from 'rxjs';
import {User, Thread, Message} from '../models';


@Component({
    inputs: ['message'],
    selector: 'chat-message',
    template: `
   <div class="msg-container"
       [ngClass]="{'base-sent': !incoming, 'base-receive': incoming}">

    <div class="avatar"
         *ngIf="!incoming">
      <img src="{{message.author.avatarSrc}}">
    </div>

    <div class="messages"
      [ngClass]="{'msg-sent': !incoming, 'msg-receive': incoming}">
      <p>{{message.text}}</p>
      <!-- <p class="time">{{message.sender}} • {{message.sentAt | fromNow}}</p> -->
      <p class="time">{{message.sender}} • {{message.sentAt | date}}</p>
    </div>

    <div class="avatar"
         *ngIf="incoming">
      <img src="{{message.author.avatarSrc}}">
    </div>
    </div>
    `
})

@NgModule({
    imports: [FormsModule],
})
export class ChatMessage implements OnInit {
    message: Message;
    currentUser: User;
    incoming: boolean;

    constructor(public userService: UserService) {
    }

    ngOnInit(): void {
    this.userService.currentUser
        .subscribe((user: User) => {
            this.currentUser = user;
            if (this.message.author && user) {
                this.incoming = this.message.author.id !== user.id;
            }
        });
    }

}