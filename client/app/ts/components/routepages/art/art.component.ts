import { Component } from '@angular/core';

declare var $: any;

@Component({
    template: `
    	<div class="container changeable-onMenuOpen changeble-onPageAppearance" id="artPage">
    		<h2 class="main-title"> <span>Art</span> </h2>
    	</div>
    `
})

export class ArtPageComponent {
	constructor() {
		$(document).ready(() => {
			$('#artPage').addClass('activePage');
		});
	}
}
