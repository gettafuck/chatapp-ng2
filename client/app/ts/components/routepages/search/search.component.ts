import { Component} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    // styles: [],
    // directives: [],
    template: `
        <div class="container changeable-onMenuOpen changeble-onPageAppearance" id="searchPage">
            <h2 class="main-title"> <span>Search</span> </h2>
            You Searched for "{{SearchText}}".
        </div>
    `
})


export class SearchPageComponent {

	private SearchText : string;

	constructor(public route: ActivatedRoute, public router: Router) {
        $(document).ready(() => {
            $('#searchPage').addClass('activePage');
        });
    }

	ngOnInit() {
        this.route.params
        .map(params => params['SearchText'])
        .subscribe((SearchText) => {
            this.SearchText = SearchText;
        });
    }
}
