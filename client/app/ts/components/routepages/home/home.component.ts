// core modules
import { Component } from '@angular/core';
import { NgModule } from '@angular/core';

import { Inject } from '@angular/core';
import { Http, HttpModule, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from "rxjs/Rx";


// services
import { UI } from '../../../services/ui.service';


// configs
import {Config} from './../../../Config';


// constants
declare var $: any;
declare var Cookies: any;


// outside components
import {ChatThreads} from '../../ChatThreads';
import {ChatNavBar} from '../../ChatNavBar';
import {ChatWindow} from '../../ChatWindow';


// current component
@Component({
    selector: 'home-component',
    template: require('./home.component.html')
})

@NgModule({
	imports: [HttpModule]
})

export class HomePageComponent {
	usersData:Array<Object>;
	http:Http;
	ui:UI;
	constructor(@Inject(Http) http:Http, ui:UI) {
		$(document).ready(() => {
			setTimeout(() => {
				$('#homePage').addClass('activePage');
			});

			console.log('Initializing in HomePageComponent:');
			ui.initSlimsroll();
		});


		this.http = http;
		http.get(Config.server + '/users')
		.subscribe((response) => {
			this.usersData = response.json();

			console.log(this.usersData[0]);
		});
	}

	public createRandomUsers = (amount:Number) => {
		console.clear();
		console.log('createRandomUsers');
	    let body = JSON.stringify({});
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let requestOptions = new RequestOptions({ headers: headers, body: body });
		this.http.post(Config.server + '/test/users/' + amount, requestOptions)
		.subscribe(
			(data) => {
				// refresh user collection
				this.http.get(Config.server + '/users')
				.subscribe((response) => {
					this.usersData = response.json();
				});

				console.log("Success for saving users");
				return true;
			},
			(error) => {
				console.error("Error for saving users");
				return Observable.throw(error);
			}
		);
	}

	public removeAllUsers = () => {
		console.clear();
	    let body = JSON.stringify({ii: 'rthg'});
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let requestOptions = new RequestOptions({ headers: headers, body: body });
		this.http.delete(Config.server + '/test/users', requestOptions)
		.subscribe(
			(data) => {
				// refresh user collection
				this.http.get(Config.server + '/users')
				.subscribe((response) => {
					this.usersData = response.json();
				});

				console.log("Success for deleting users");
				return true;
			},
			(error) => {
				console.error("Error for deleting users");
				return Observable.throw(error);
			}
		);
	}
}