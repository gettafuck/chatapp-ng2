// core modules
import {Component} from '@angular/core';
import {NgModule} from '@angular/core';

import {Inject, Input} from '@angular/core';
import {Http, HttpModule, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Rx";


// services
import {UI} from '../../../services/ui.service';


// configs
import {Config} from './../../../Config';


// constants
declare var $: any;
declare var Cookies: any;


// outside components
//import {TestCompont} from './test.component';


// current component
@Component({
    selector: 'login-component',
    template: require('./login.component.html')
})

@NgModule({
	imports: [HttpModule]
})

export class LoginPageComponent {
	@Input() userLogin;
	@Input() userPassword;

	http:Http;
	ui:UI;

    public loggedUser:any;

	loginStatus:String;

	constructor(@Inject(Http) http:Http, ui:UI) {
		let loggedUserStr = Cookies.get('loggedUser') || '{}';
		this.loggedUser = JSON.parse(loggedUserStr);
		console.log('logged user: ' + this.loggedUser.name + ', role: ' + this.loggedUser.role);

		if(this.loggedUser._id) location.replace('/#/home');

		this.http = http;
		this.ui = ui;

		$(document).ready(() => {
			setTimeout(() => {
				$('#loginPage').addClass('activePage');
			});

			console.log('Initializing in LoginPageComponent:');
			ui.initSlimsroll();
		});
	}

	public login() {
		console.clear();

	    let body = JSON.stringify({
	    	login: this.userLogin,
	    	password: this.userPassword
	    });

		let headers = new Headers({ 'Content-Type': 'application/json' });
		let requestOptions = new RequestOptions({headers: headers});

		this.http.post(Config.server + '/login', body, requestOptions)
 		.map(response => response.json())
		.subscribe(
			(response:Response) => {
				console.log('Login status: Success.');
				console.log(response);
				this.loginStatus = 'You are successfully logged in';

				let loggedUserStr = JSON.stringify(response);
				Cookies.set('loggedUser', loggedUserStr);

				setTimeout(() => {
					location.replace('/#/home');
					location.reload();
				}, 1000);

				return true;
			},
			(error:Response) => {
				console.log('Cannot login.');
				console.log(error);
				this.loginStatus = 'Wrong login or password';
				return Observable.throw(error);
			}
		);
	}
}