// core modules
import {Component, Input, Output} from '@angular/core';
import {NgModule} from '@angular/core';

import {Inject} from '@angular/core';
import {Http, HttpModule, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Rx";


// services
import { UI } from '../../../services/ui.service';


// configs
import {Config} from './../../../Config';


// constants
declare var $: any;
declare var Cookies: any;

declare var io: any;
let socket = io('http://localhost:2222/chat');


// outside components
import {ChatThreads} from '../../ChatThreads';
import {ChatNavBar} from '../../ChatNavBar';
import {ChatWindow} from '../../ChatWindow';


// current component
@Component({
    selector: 'inbox-component',
    template: require('./inbox.component.html')
})

@NgModule({
	imports: [HttpModule]
})

export class InboxPageComponent {
	http:Http;
	ui:UI;

	@Input() inputMessageValue;
	chatMessages:Array<any>;

	constructor(@Inject(Http) http:Http, ui:UI) {
		this.http = http;
		this.ui = ui;

		this.chatMessages = [];

		$(document).ready(() => {
			setTimeout(() => {
				$('#inboxPage').addClass('activePage');
			});

			console.log('Initializing in InboxPageComponent:');
			ui.initSlimsroll();
		});
 

		socket.on('serverMessage', (response) => {
			this.chatMessages.push(response.message);
			// console.log(this.chatMessages);
			console.log(response);
		});
		// fix messages re-rendering
		setInterval(() => {
			this.chatMessages = this.chatMessages;
		}, 100);
	}

	public sendMessage() {
		socket.emit('clientMessage', {
			message: this.inputMessageValue
		});
		this.inputMessageValue = '';
	}
}