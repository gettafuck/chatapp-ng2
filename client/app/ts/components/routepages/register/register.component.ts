// core modules
import {Component} from '@angular/core';
import {NgModule} from '@angular/core';

import {Inject, Input} from '@angular/core';
import {Http, HttpModule, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Rx";


// services
import {UI} from '../../../services/ui.service';


// configs
import {Config} from './../../../Config';


// constants
declare var $: any;
declare var Cookies: any;


// outside components
//import {TestCompont} from './test.component';


// current component
@Component({
    selector: 'register-component',
    template: require('./register.component.html')
})

@NgModule({
	imports: [HttpModule]
})

export class RegisterPageComponent {
	@Input() userLogin;
	@Input() userPassword;
	@Input() userEmail;
	@Input() userName;
	@Input() userSecondName; // not required
	@Input() userSurName;
	@Input() userImgUrl; // not required

	http:Http;
	ui:UI;

    public loggedUser:any;

	registerStatus:String;

	constructor(@Inject(Http) http:Http, ui:UI) {
		let loggedUserStr = Cookies.get('loggedUser') || '{}';
		this.loggedUser = JSON.parse(loggedUserStr);
		console.log('logged user: ' + this.loggedUser.name + ', role: ' + this.loggedUser.role);

		if(this.loggedUser._id) location.replace('/#/home');

		this.http = http;
		this.ui = ui;

		$(document).ready(() => {
			setTimeout(() => {
				$('#registerPage').addClass('activePage');
			});

			console.log('Initializing in RegisterPageComponent:');
			ui.initSlimsroll();
		});
	}


	public register() {
		console.clear();

		let expression = (!this.userLogin ||
						  !this.userPassword ||
						  !this.userEmail ||
						  !this.userName ||
						  !this.userSurName);
		if(expression) this.registerStatus = 'One of required fields is empty';
		else {
			let minLoginWidth = 4;
			let minPasswordWidth = 8;
			let emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			let userEmail = this.userEmail;
			let emailCorrect = userEmail.match(emailRegExp);

			if( !emailCorrect )
				this.registerStatus = 'Wrong e-mail format';
			else if( this.userLogin.length < minLoginWidth )
				this.registerStatus = 'Too short login, min amount of characters is ' + minLoginWidth;
			else if( this.userPassword.length < minPasswordWidth )
				this.registerStatus = 'Too short password, min amount of characters is ' + minPasswordWidth;
			else {
				this.registerStatus = '';

				let body = JSON.stringify({
					login: this.userLogin,
					password: this.userPassword,
					contacts: {
						email: this.userEmail
					},
					name: this.userName,
					secondName: this.userSecondName,
					surName: this.userSurName,
					imgUrl: this.userImgUrl
				});

				let headers = new Headers({ 'Content-Type': 'application/json' });
				let requestOptions = new RequestOptions({headers: headers});

				this.http.post(Config.server + '/user', body, requestOptions)
					.map(response => response.json())
				.subscribe(
					(response:Response) => {
						console.log('Register status: Success.');
						console.log(response);
						this.registerStatus = 'You are successfully registered';

						setTimeout(() => {
							location.replace('/#/home');
							location.reload();
						}, 1000);

						return true;
					},
					(error:Response) => {
						console.log('Cannot register.');
						console.log(error);
						this.registerStatus = 'Register error: maybe you input existed login';
						return Observable.throw(error);
					}
				);
			}
		}
	}
}