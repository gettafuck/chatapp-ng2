// core modules
import {Component} from '@angular/core';
import {NgModule} from '@angular/core';

import {Inject, Input, Output, EventEmitter} from '@angular/core';
import {Http, HttpModule, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Rx";


// services
import {UI} from '../../services/ui.service';


// configs
import {Config} from './../../Config';


// constants
declare var $: any;
declare var Cookies: any;


// outside components
//import {TestCompont} from './test.component';


@Component({
    selector: 'navbar',
    template : require('./navbar.component.html')
})
export class NavbarComponent {
    private HideExpandedNavBar: boolean = true;
    private HideSearchInput: boolean = true;

    private innerSearchInputText: string = '';

    private NavbarSearch: any = {};

    @Input() NavbarBrand : string;
    @Input() NavbarItems : Array<any>;

    @Output() NavbarSearchSubmit = new EventEmitter<any>();

    http:Http;
    public ui:UI;

    public Config:any;

    public loggedUser:any;

	constructor(@Inject(Http) http:Http, ui:UI) {
        this.http = http;
        this.ui = ui;

        this.Config = Config;

        let loggedUserStr = Cookies.get('loggedUser') || '{}';
        this.loggedUser = JSON.parse(loggedUserStr);
        console.log('logged user: ' + this.loggedUser.name + ', role: ' + this.loggedUser.role);

        console.clear();
        console.log('logged user token: ' + this.loggedUser.token);

        this.NavbarItems = [
            {
                label : 'Home',
                href : 'home',
                pageId: 'homePage'
            },
            {
                label : 'Inbox',
                href : 'inbox',
                pageId: 'inboxPage'
            }
        ];

        console.log('Initializing in NavbarComponent:');
        ui.initSlimsroll();
	}

    public logout() {
        console.clear();
        console.log(this.loggedUser.token);

        let body = JSON.stringify({
            token: this.loggedUser.token
        });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let requestOptions = new RequestOptions({headers: headers});

        this.http.post(Config.server + '/logout', body, requestOptions)
        //.map(response => response.json())
        .subscribe(
            (response:Response) => {
                console.log('Logout status: Success.');
                console.log(response);

                Cookies.remove('loggedUser');

                setTimeout(() => {
                    location.replace('/#/login');
                    location.reload();
                }, 1000);

                return true;
            },
            (error:Response) => {
                console.log('Cannot logout.');
                console.log(error);

                return Observable.throw(error);
            }
        );
    }




	closeOnMouseOut(idx:number) {
        if (idx == this.NavbarItems.length-1) {
            this.HideExpandedNavBar = true;
        }
	}

	onSearchSubmit(formvalue:any) {
		//console.log(formvalue);
        this.NavbarSearchSubmit.emit(formvalue.SearchText);
        formvalue.SearchText = "";
	}

    handleMenuClick(event:any) {
        this.HideExpandedNavBar = true;
    }

}