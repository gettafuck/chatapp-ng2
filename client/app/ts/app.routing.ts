import {Routes, RouterModule}  from '@angular/router';


import {HomePageComponent}  from './components/routepages/home/home.component';
import {LoginPageComponent}  from './components/routepages/login/login.component';
import {RegisterPageComponent} from './components/routepages/register/register.component';
import {InboxPageComponent} from './components/routepages/inbox/inbox.component';

import {ArtPageComponent}  from './components/routepages/art/art.component';
import {WritingPageComponent}  from './components/routepages/writing/writing.component';
import {SearchPageComponent}  from './components/routepages/search/search.component';


const appRoutes: Routes = [
    {path: 'home', component: HomePageComponent},
    {path: 'login', component: LoginPageComponent},
    {path: 'register', component: RegisterPageComponent},
    {path: 'inbox', component: InboxPageComponent},

    {path: 'art', component: ArtPageComponent},
    {path: 'writing', component: WritingPageComponent },
    {path: 'search/:SearchText', component: SearchPageComponent  },
    {path: '**', component: HomePageComponent    }
];

export const appRoutingProviders: any[] = [];

export const routing = RouterModule.forRoot(appRoutes);
