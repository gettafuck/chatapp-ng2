'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	role: String,
	verified: Boolean,
	login: { type: String, unique: true, required: true, dropDups: true },
	passwordHash: String,
	name: String,
	secondName: String,
	surName: String,
	imgUrl: String,
	city: String,
	contacts: {
		skype: String,
		phone: String,
		email: String
	},

	salt: String,
	token: String,

	registrationDate: { type: Date, default: Date.now },

	birthday: Date
});

const User = mongoose.model('User', UserSchema);

module.exports = User;